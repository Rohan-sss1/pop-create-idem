import pathlib


def run(hub, ctx, root_directory: pathlib.Path):
    """
    This is purposefully left as no-op. When used, no code will be rendered but default templates will be provided.
    This is only applicable for "swagger" or "openapi3" specifications.
    """
