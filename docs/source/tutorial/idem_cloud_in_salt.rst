https://docs.saltproject.io/en/master/ref/modules/all/salt.modules.idem.html
https://docs.saltproject.io/en/latest/ref/states/all/salt.states.idem.html

SALT.MODULES.IDEM
=================

IDEM SUPPORT
------------
This module provides access to idem execution modules

New in version 3002.

.. code-block:: python

    salt.modules.idem.exec_(
        path, acct_file=None, acct_key=None, acct_profile=None, *args, **kwargs
    )

Call an idem execution module

path
The idem path of the idem execution module to run

acct_file
Path to the acct file used in generating idem ctx parameters. Defaults to the value in the ACCT_FILE environment variable.

acct_key
Key used to decrypt the acct file. Defaults to the value in the ACCT_KEY environment variable.

acct_profile
Name of the profile to add to idem's ctx.acct parameter. Defaults to the value in the ACCT_PROFILE environment variable.

args
Any positional arguments to pass to the idem exec function

kwargs
Any keyword arguments to pass to the idem exec function

CLI Example:

.. code-block:: bash

    salt '*' idem.exec test.ping

Maturity
new

Depends
acct, pop, pop-config, idem

Platform
all

SALT.STATES.IDEM
================

IDEM SUPPORT
------------
This state provides access to idem states

New in version 3002.

.. code-block:: python

        salt.states.idem.state(
            name,
            sls,
            acct_file=None,
            acct_key=None,
            acct_profile=None,
            cache_dir=None,
            render=None,
            runtime=None,
            source_dir=None,
            test=False,
        )

Execute an idem sls file through a salt state

sls
A list of idem sls files or sources

acct_file
Path to the acct file used in generating idem ctx parameters. Defaults to the value in the ACCT_FILE environment variable.

acct_key
Key used to decrypt the acct file. Defaults to the value in the ACCT_KEY environment variable.

acct_profile
Name of the profile to add to idem's ctx.acct parameter Defaults to the value in the ACCT_PROFILE environment variable.

cache_dir
The location to use for the cache directory

render
The render pipe to use, this allows for the language to be specified (jinja|yaml)

runtime
Select which execution runtime to use (serial|parallel)

source_dir
The directory containing sls files

.. code-block:: yaml

    cheese:
        idem.state:
            - runtime: parallel
            - sls:
                - idem_state.sls
                - sls_source

Maturity
new

Depends
acct, pop, pop-config, idem

Platform
all
